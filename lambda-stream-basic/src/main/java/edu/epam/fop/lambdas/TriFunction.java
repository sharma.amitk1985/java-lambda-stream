package edu.epam.fop.lambdas;

import java.util.function.Function;

public interface TriFunction<X,Y,Z,P> {
    P apply(TriFunction<X,Y,Z,P> func);
}
