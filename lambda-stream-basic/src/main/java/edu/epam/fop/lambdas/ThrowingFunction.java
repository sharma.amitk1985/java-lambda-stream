package edu.epam.fop.lambdas;

import java.util.function.Function;
@FunctionalInterface
public interface ThrowingFunction<U,T,P>{
    T apply( U u) throws Throwable;
    static Function quite(ThrowingFunction tf){

        if(tf == null){
            return null;
        }
        return null;
    }

}
